import numpy as np
from keras import backend
from keras.applications.vgg19 import VGG19
from keras.applications.vgg16 import VGG16
import cv2
import os
import time
from scipy.optimize import fmin_l_bfgs_b

HEIGHT = 300
WIDTH = 300

# weight set for each error term
CONTENT_LOSS_WEIGHT = 0.025
STYLE_LOSS_WEIGHT = 5.0
DENOISE_LOSS_WEIGHT = 1.0

def get_content_loss(content, mixed):
    """
    content loss is the squared-error loss (or Euclidean distance) between the
    representations of content and mixed images respected to a certain layer
    """
    return backend.sum(backend.square(mixed - content))

def add_content_loss_to_total(layers, layer_names, total_loss):
    """
    add the content loss from each layer to total_loss
    """
    for layer_name in layer_names:
        layer_features = layers[layer_name]
        content_features = layer_features[0, :, :, :]
        mixed_features = layer_features[2, :, :, :]
        total_loss += CONTENT_LOSS_WEIGHT * get_content_loss(content_features, mixed_features)

    return total_loss
    

def Gram_matrix(x):
    """
    x: reference texture

    Gram matrix is used generate texture that is similar to reference texture x.
    (More about Gram matrix: https://arxiv.org/pdf/1606.01286.pdf)
    """
    features = backend.batch_flatten(backend.permute_dimensions(x, (2, 0, 1)))
    return backend.dot(features, backend.transpose(features))

def get_style_loss(style, mixed, num_of_channels = 3):
    """
    style loss is the mean squared error between the entries of the Gram matrix from
    style image and the generated image respected to a certain layer.
    """
    gram_style = Gram_matrix(style)
    gram_mixed = Gram_matrix(mixed)
    size = HEIGHT * WIDTH

    return backend.sum(backend.square(gram_style - gram_mixed)) / (4 * (num_of_channels ** 2) * (size ** 2))

def add_style_loss_to_total(layers, layer_names, total_loss):
    """
    add the style loss from each layer to total loss
    """
    for layer_name in layer_names:
        layer_features = layers[layer_name]
        style_features = layer_features[1, :, :, :]
        mixed_features = layer_features[2, :, :, :]
        sl = get_style_loss(style_features, mixed_features)
        total_loss += (STYLE_LOSS_WEIGHT / len(layer_names)) * sl

    return total_loss

def get_denoise_loss(x):
    """
    This is regularization term to spatially smooth x
    """
    a = backend.square(x[:, :HEIGHT - 1, : WIDTH - 1, :] - x[:, 1:, : WIDTH - 1, :])
    b = backend.square(x[:, :HEIGHT - 1, : WIDTH - 1, :] - x[:, : HEIGHT - 1, 1 : , :])
    return backend.sum(backend.pow(a + b, 1.25))

def prepare_image(path, resizing = False, display = False):
    """
    Function to load and prepare image for tensor input
    """
    image = cv2.imread(path)

    if image is None:
        print ("Fail to load an image from a path ", path)
        return None

    if resizing is True:
        image = cv2.resize(image,
                           (WIDTH, HEIGHT), 
                           interpolation = cv2.INTER_CUBIC)

    if display is True:
        cv2.imshow(path, image)
        cv2.waitKey()

    image = image.astype(float)

    # preprocess image to use in VGG model
    # 1. Expand another dimension to an image
    # 2. subtract RGB mean value from each channel
    # 3. flip RGB to BGR
    image = np.expand_dims(image, axis=0)
    image[:, :, :, 0] -= 103.939
    image[:, :, :, 1] -= 116.779
    image[:, :, :, 2] -= 123.68
    image = image[:, :, :, ::-1]

    return image

def get_image_back(data):
    """
    Function to reconstruct image after mixing process
    """
    image = data.copy()
    image = image.reshape((HEIGHT, WIDTH, 3))
    image = image[:, :, ::-1]
    image[:, :, 0] += 103.939
    image[:, :, 1] += 116.779
    image[:, :, 2] += 123.68
    
    return np.clip(image, 0, 255).astype('uint8')

def get_input_tensor(content_path, style_path, resizing = True, display = False):
    content_image = prepare_image(content_path, resizing = resizing, display = display)

    style_image = prepare_image(style_path, resizing = resizing, display = display)

    content_image = backend.variable(content_image)
    style_image = backend.variable(style_image)
    mixed_image = backend.placeholder((1, HEIGHT, WIDTH, 3))

    input_tensor = backend.concatenate([content_image,
                                style_image,
                                mixed_image], axis=0)

    return input_tensor, mixed_image

def get_model(model_name, input_tensor):
    if model_name is 'VGG16':
        return VGG16(input_tensor = input_tensor, 
                     weights = 'imagenet',
                     include_top = False)
    else:
        return VGG19(input_tensor = input_tensor, 
                     weights = 'imagenet',
                     include_top = False)

input_tensor, mixed_image = get_input_tensor('zlatan.jpg', 'style2.jpeg')

model = get_model('VGG16', input_tensor)

layers = dict([(layer.name, layer.output) for layer in model.layers])

# here is the linear combination of all error terms
total_loss = backend.variable(0.0)

# Should experiment with different layer features
content_layers = ['block1_conv2', 'block2_conv2',
                  'block3_conv3', 'block4_conv3',
                  'block5_conv3']

# add content loss to total loss
total_loss = add_content_loss_to_total(layers, content_layers, total_loss)

# feature layer for style loss
style_layers = ['block1_conv2', 'block2_conv2',
                  'block3_conv3', 'block4_conv3',
                  'block5_conv3']

# compute and add each style loss from each feature layer to total loss
total_loss = add_style_loss_to_total(layers, style_layers, total_loss)

# add the denoise loss to total loss
total_loss += DENOISE_LOSS_WEIGHT * get_denoise_loss(mixed_image)

gradients = backend.gradients(total_loss, mixed_image)

outputs = [total_loss]
outputs += gradients
f_outputs = backend.function([mixed_image], outputs)

def compute_loss_and_gradients(x):
    x = x.reshape((1, HEIGHT, WIDTH, 3))
    results = f_outputs([x])

    # return loss and gradients
    loss = results[0]
    grads = results[1].flatten().astype('float64')

    return loss, grads

class Evaluator(object):
    def __init__(self):
        self.loss = None
        self.gradients = None

    def get_loss(self, x):
        assert self.loss is None
        loss_val, grad_vals = compute_loss_and_gradients(x)
        self.loss = loss_val
        self.gradients = grad_vals

        return self.loss

    def get_grads(self, x):
        assert self.loss is not None
        grad_vals = np.copy(self.gradients)
        self.loss = None
        self.gradients = None
        return grad_vals

evaluator = Evaluator()

# initiate mixed image
mixed = np.random.uniform(0, 255, (1, HEIGHT, WIDTH, 3)) - 128.

iterations = 20

for i in range(iterations):
    print('Running iteration ', i)
    start_time = time.time()

    mixed, min_val, info = fmin_l_bfgs_b(evaluator.get_loss, mixed.flatten(),
                                     fprime=evaluator.get_grads, maxfun=20)

    print('Current loss value:', min_val)
    end_time = time.time()
    print('Iteration %d completed in %ds' % (i, end_time - start_time))

    mixed_image = get_image_back(mixed)
    save_file_name = 'i20_b3_zlatan_' + str(i) + '.jpg'
    cv2.imwrite(save_file_name, mixed_image)

    


        









    






