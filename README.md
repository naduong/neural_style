# README #

### What is this repository for? ###
The purpose of this project is to get myself be familiar with tensorflow, keras and some famous Convolutional Neural Network models, such as VGG's, Inception. 

A few codes are borrowed from keras's tutorial on style transfer (https://github.com/keras-team/keras/blob/master/examples/neural_style_transfer.py).

If you want to know more about style transfer, please read https://arxiv.org/abs/1508.06576

### How do I get set up? ###
Dependencies: tensorflow, keras, opencv3

### Some result: ###
Content image: ![Scheme](imgs/zlatan.jpg)

Style image: ![Scheme](imgs/style2_res.jpg)

vgg16 output: ![Scheme](imgs/zlatan_vgg16.gif)

vgg19 output: ![Scheme](imgs/zlatan_vgg19.gif)

Inception output: ![Scheme](imgs/zlatan_inceptionv3.gif)
